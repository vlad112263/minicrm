<?php
namespace common\models\utils;


use common\helpers\ArrayHelper;

class Request
{
    public const REQUEST = 0;
    public const GET     = 1;
    public const POST    = 2;

    public static function post($name = null, $defaultValue = null)
    {
        $post = \Yii::$app->request->post();

        if ($name === null) {
            return $post;
        }

        foreach (ArrayHelper::wrap($name) as $_name) {
            if (! is_null($value = ArrayHelper::getValue($post, $_name))) {
                return $value;
            }
        }

        return $defaultValue;
    }

    public static function get($name = null, $defaultValue = null)
    {
        $get = \Yii::$app->request->get();

        if ($name === null) {
            return $get;
        }

        foreach (ArrayHelper::wrap($name) as $_name) {
            if (! is_null($value = ArrayHelper::getValue($get, $_name))) {
                return $value;
            }
        }

        return $defaultValue;
    }

    public static function request($name = null, $defaultValue = null)
    {
        $request = $_REQUEST;

        if ($name === null) {
            return $request;
        }

        foreach (ArrayHelper::wrap($name) as $_name) {
            if (! is_null($value = ArrayHelper::getValue($request, $_name))) {
                return $value;
            }
        }

        return $defaultValue;
    }

    public static function destructuring($args, $type = self::REQUEST): array
    {
        $array = [];

        if (in_array($type, [self::REQUEST, 'request'])) {
            $data = $_REQUEST;
        } elseif (in_array($type, [self::GET, 'get'])) {
            $data = $_GET;
        } elseif (in_array($type, [self::POST, 'post'])) {
            $data = $_POST;
        } else {
            $data = [];
        }

        foreach ($args as $arg) {
            if ($value = ArrayHelper::getValue($data, $arg)) {
                $array[$arg] = $value;
            }
        }

        return $array;
    }

    public static function isAjaxAndPost(): bool
    {
        return self::isAjax() && self::isPost();
    }

    public static function isAjaxAndGet(): bool
    {
        return self::isAjax() && self::isGet();
    }

    public static function isAjax(): bool
    {
        return \Yii::$app->request->isAjax;
    }

    public static function isPost(): bool
    {
        return \Yii::$app->request->isPost;
    }

    public static function isGet(): bool
    {
        return \Yii::$app->request->isGet;
    }
}
