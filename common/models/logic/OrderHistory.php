<?php
namespace common\models\logic;

use common\models\db\OrderHistoryDb;

/**
 * Class OrderHistory
 * @package common\models\logic
 */
class OrderHistory extends OrderHistoryDb
{

    /**
     * @inheritDoc
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->old_data = unserialize($this->old_data);
        $this->data     = unserialize($this->data);
    }

    public function beforeSave($insert)
    {
        $this->old_data = serialize($this->old_data);
        $this->data     = serialize($this->data);
        return parent::beforeSave($insert);
    }

    public function getFormattedValue($attribute, $value)
    {
        if ($attribute == 'created_at') {
            return \Yii::$app->formatter->asDate($value, 'php:d M, y H:i');
        }
        if ($attribute == 'product_id') {
            return Product::find()->select('name')->andWhere(['id' => $value])->limit(1)->scalar();
        }
        return $value;
    }

}

