<?php
namespace common\models\logic;

use common\models\db\OrderDb;

/**
 * Class Order
 * @package common\models\logic
 *
 * @property-read string $statusLabel
 */
class Order extends OrderDb
{

    public static function statusLabels(): array
    {
        return [
            self::STATUS_ACCEPT => \Yii::t('app', 'Accept'),
            self::STATUS_DENIED => \Yii::t('app', 'Denied'),
            self::STATUS_DEFECT => \Yii::t('app', 'Defect'),
        ];
    }

    public function getStatusLabel(): string
    {
        return self::statusLabels()[$this->status];
    }


}
