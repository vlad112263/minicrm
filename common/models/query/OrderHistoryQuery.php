<?php
namespace common\models\query;

use common\models\logic\OrderHistory;
use yii\db\ActiveQuery;


class OrderHistoryQuery extends ActiveQuery
{
    /**
     * @param null $db
     * @return array|OrderHistory|null
     */
    public function one($db = null)
    {
        $this->limit(1);
        return parent::one($db);
    }

    /**
     * @param null $db
     * @return array|OrderHistory[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }


}
