<?php
namespace common\models\query;

use common\models\logic\Order;


class OrderQuery extends \yii\db\ActiveQuery
{

    /**
     * @param null $db
     * @return array|Order|null
     */
    public function one($db = null)
    {
        $this->limit(1);
        return parent::one($db);
    }

    /**
     * @param null $db
     * @return array|Order[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }


}
