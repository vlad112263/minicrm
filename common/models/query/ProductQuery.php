<?php
namespace common\models\query;

use common\models\logic\Product;
use yii\db\ActiveQuery;


class ProductQuery extends ActiveQuery
{
    /**
     * @param null $db
     * @return array|Product|null
     */
    public function one($db = null)
    {
        $this->limit(1);
        return parent::one($db);
    }

    /**
     * @param null $db
     * @return array|Product[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }


}
