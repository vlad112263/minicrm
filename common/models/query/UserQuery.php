<?php
namespace common\models\query;

use common\models\logic\User;
use yii\db\ActiveQuery;


class UserQuery extends ActiveQuery
{

    public function active()
    {
        return $this->andWhere(['status' => User::STATUS_ACTIVE]);
    }

    /**
     * @param null $db
     * @return array|User|null
     */
    public function one($db = null)
    {
        $this->limit(1);
        return parent::one($db);
    }

    /**
     * @param null $db
     * @return array|User[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

}
