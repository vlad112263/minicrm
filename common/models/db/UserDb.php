<?php
namespace common\models\db;

use common\models\query\UserQuery;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 *
 * @property integer $id
 * @property string  $password_hash
 * @property string  $email
 * @property integer $role
 * @property integer $status
 * @property integer $deleted_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property string  $password
 *
 * @property-read string $statusLabel
 * @property-read string $roleLabel
 */
class UserDb extends ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE   = 1;

    const ROLE_MANAGER = 1;
    const ROLE_ADMIN   = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['role', 'default', 'value' => self::ROLE_MANAGER],
            ['role', 'in', 'range' => [self::ROLE_MANAGER, self::ROLE_ADMIN]],
            ['status', 'default', 'value' => self::STATUS_INACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE]],
        ];
    }

    /**
     * @return UserQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return (new UserQuery(get_called_class()))->andWhere(['deleted_at' => null]);
    }

    /**
     * @inheritDoc
     */
    public function delete()
    {
        return $this->updateAttributes([
            'deleted_at' => time()
        ]);
    }

    /**
     * @return array
     */
    public static function statusLabels(): array
    {
        return [
            self::STATUS_INACTIVE => \Yii::t('app', 'Inactive'),
            self::STATUS_ACTIVE   => \Yii::t('app', 'Active'),
        ];
    }

    public static function roleLabels(): array
    {
        return [
            self::ROLE_MANAGER => \Yii::t('app', 'Manager'),
            self::ROLE_ADMIN   => \Yii::t('app', 'Admin'),
        ];
    }

    public function getStatusLabel(): string
    {
        return self::statusLabels()[$this->status];
    }

    public function getRoleLabel(): string
    {
        return self::roleLabels()[$this->role];
    }

}
