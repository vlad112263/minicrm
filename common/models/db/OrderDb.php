<?php
namespace common\models\db;

use common\models\logic\Product;
use common\models\query\OrderQuery;
use common\models\query\ProductQuery;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;


/**
 * Class OrderDb
 * @package common\models\db
 *
 *
 * @property integer $id
 * @property string  $client_name
 * @property string  $client_phone
 * @property string  $comment
 * @property integer $product_id
 * @property double  $total
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Product $product
 */
class OrderDb extends ActiveRecord
{
    const STATUS_DENIED = 0;
    const STATUS_ACCEPT = 1;
    const STATUS_DEFECT = 2;

    /**
     * @return array
     */
    public function statusList(): array
    {
        return [
            self::STATUS_DENIED,
            self::STATUS_ACCEPT,
            self::STATUS_DEFECT,
        ];
    }

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class
        ];
    }


    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['client_name', 'client_phone'], 'required'],

            [['client_name', 'client_phone'], 'string'],
            ['product_id', 'integer'],
            ['total', 'double', 'min' => 0],

            ['product_id', 'exist', 'skipOnEmpty' => false, 'skipOnError' => false, 'targetClass' => Product::class, 'targetAttribute' => ['product_id' => 'id']],

            ['status', 'default', 'value' => self::STATUS_ACCEPT],
            ['status', 'in', 'range' => self::statusList()],

            [['created_at', 'updated_at', 'comment'], 'safe'],
        ];
    }


    /**
     * @return OrderQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return new OrderQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery|ProductQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }

}
