<?php
namespace common\models\db;

use common\models\logic\Order;
use common\models\logic\User;
use common\models\query\OrderHistoryQuery;
use common\models\query\OrderQuery;
use common\models\query\UserQuery;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;


/**
 * Class OrderHistoryDb
 * @package common\models\db
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $order_id
 * @property mixed   $old_data
 * @property mixed   $data
 * @property integer $created_at
 *
 * @property Order $order
 * @property User  $user
 */
class OrderHistoryDb extends ActiveRecord
{
    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%order_history}}';
    }


    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['user_id', 'order_id'], 'integer'],

            [['old_data', 'data'], 'safe']
        ];
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ]
            ]
        ];
    }

    /**
     * @return OrderHistoryQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return new OrderHistoryQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery|OrderQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::class, ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

}
