<?php
namespace common\models\db;

use common\models\query\ProductQuery;
use yii\db\ActiveRecord;


/**
 * Class ProductDb
 * @package common\models\db
 *
 * @property integer $id
 * @property string  $name
 * @property double  $price
 */
class ProductDb extends ActiveRecord
{
    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%product}}';
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            ['name', 'string', 'max' => 255],
            ['price', 'double']
        ];
    }

    /**
     * @return ProductQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }


}
