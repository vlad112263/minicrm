<?php
namespace common\controllers\ajax;

use common\models\utils\Request;
use ReflectionClass;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class AjaxController
 * @package common\controllers\ajax
 */
class AjaxController extends Controller
{
    /**
     * {@inheritDoc}
     */
    public function beforeAction($action)
    {
        if (Yii::$app->request->isAjax) {
            $this->enableCsrfValidation = false;
            Yii::$app->response->format = Response::FORMAT_JSON;

            return parent::beforeAction($action);
        }

        throw new BadRequestHttpException();
    }

    /**
     * @return array
     */
    public function actionValidate()
    {
        $reflection = (new ReflectionClass(Request::request('class', null)));

        if ($params = Request::request('params')) {
            $model = $reflection->newInstanceArgs($params);
        } elseif ($config = Request::request('config')) {
            $model = $reflection->newInstance($config);
        } else {
            $model = $reflection->newInstance();
        }

        $formName = $model->formName();

        $model->scenario = Request::post(['scenario', "{$formName}[scenario]"], $model->scenario);

        $model->load(Request::post());

        return ActiveForm::validate($model);
    }

}
