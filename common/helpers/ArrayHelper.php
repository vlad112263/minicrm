<?php

namespace common\helpers;


class ArrayHelper extends \yii\helpers\ArrayHelper
{

    public static function wrap($value)
    {
        return is_array($value) ? $value : array($value);
    }


}
