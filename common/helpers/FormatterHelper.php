<?php
namespace common\helpers;


class FormatterHelper
{
    public static function isEqual($val1, $val2): bool
    {
        if (gettype($val1) === gettype($val2)) {
            return $val1 === $val2;
        } else {

            if (is_string($val1) && is_string($val2)) {
                return $val1 === $val2;
            }

            if (is_numeric($val1) && is_numeric($val2)) {
                return $val1 == $val2;
            }
        }
        return (string)$val1 === (string)$val2;
    }

    public static function isDifferent($val1, $val2): bool
    {
        return ! self::isEqual($val1, $val2);
    }
}
