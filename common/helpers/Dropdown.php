<?php
namespace common\helpers;



use common\models\logic\Order;
use common\models\logic\Product;
use common\models\logic\User;

class Dropdown
{
    public static function orderStatuses(string $nullItem = ''): array
    {
        return self::_return(Order::statusLabels(), $nullItem);
    }

    public static function users(string $nullItem = ''): array
    {
        $items = ArrayHelper::map(
            User::find()->active()->asArray()->all(),
            'id',
            'email'
        );

        return self::_return($items, $nullItem);
    }

    public static function products(string $nullItem = ''): array
    {
        $items = ArrayHelper::map(
            Product::find()->asArray()->orderBy(['name' => SORT_ASC])->asArray()->all(),
            'id',
            'name'
        );

        return self::_return($items, $nullItem);
    }

    protected static function _return($values, string $nullItem = '')
    {
        if ($nullItem) {
            return ArrayHelper::merge([null => $nullItem], $values);
        }

        return $values;
    }
}
