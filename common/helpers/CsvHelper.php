<?php
namespace common\helpers;


use yii\web\Response;

class CsvHelper
{

    public static function render(array $headers, array $data)
    {
        $output = implode(';', $headers) . "\r\n" ;

        foreach ($data as $val) {
            $output .= implode(';', $val) . "\r\n" ;
        }

        $output = iconv('utf-8', 'windows-1251', $output);

        $response = \Yii::$app->response;
        $response->format = Response::FORMAT_RAW;
        $headers = $response->getHeaders();

        $headers->set('Content-Type', 'text/csv');
        $headers->set('Content-Disposition', 'attachment; filename="export_' . date('d.m.Y') . '.csv"');

        return $output;
    }

}
