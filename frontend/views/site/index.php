<?php

/* @var $this yii\web\View */
/* @var $model \frontend\models\forms\OrderForm */

use frontend\controllers\ajax\AjaxController;
use lavrentiev\widgets\toastr\Notification;
use yii\helpers\Url;

$this->title = 'My Yii Application';

?>
<div class="site-index">

    <?php $form = \yii\widgets\ActiveForm::begin([
        'id'                     => 'order-form',
        'enableAjaxValidation'   => true,
        'enableClientValidation' => false,
        'validateOnBlur'         => false,
        'validateOnChange'       => false,
        'validationUrl'          => Url::to([AjaxController::VALIDATION_URL, 'class' => \frontend\models\forms\OrderForm::class]),
        'fieldConfig' => [
            'template' => "{label}\n{input}\n{hint}\n{error}",
        ],
    ]) ?>

    <?php
        if ($flash = Yii::$app->session->getFlash(\common\enums\ESessionKeys::ORDER_SUCCESS)) {
            Notification::widget([
                'type' => 'success',
                'title' => 'Success',
                'message' => $flash
            ]);
        }
    ?>

    <?= $form->field($model, 'name')->textInput()->label(Yii::t('app', 'Your name')) ?>

    <?= $form->field($model, 'phone')->widget(\borales\extensions\phoneInput\PhoneInput::class, [
        'jsOptions' => [
            'allowExtensions'    => true,
            'nationalMode'       => false,
            'preferredCountries' => ['ru'],
        ]
    ])->label(Yii::t('app', 'Your phone')) ?>

    <?= $form->field($model, 'comment')->textarea()->label(Yii::t('app', 'Enter comment')) ?>

    <?= $form->field($model, 'product')->dropDownList(\common\helpers\Dropdown::products(Yii::t('app', '-- SELECT PRODUCT --'))) ?>

    <?= \yii\helpers\Html::submitButton('Confirm', ['class' => 'btn btn-primary']) ?>


    <?php \yii\widgets\ActiveForm::end() ?>
</div>
