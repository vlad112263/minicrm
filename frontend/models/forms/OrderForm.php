<?php

namespace frontend\models\forms;

use borales\extensions\phoneInput\PhoneInputValidator;
use common\models\logic\Order;
use common\models\logic\Product;
use Yii;
use yii\base\Model;

/**
 * OrderForm is the model behind the contact form.
 */
class OrderForm extends Model
{
    public $name;
    public $phone;
    public $comment;

    public $product;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['name', 'phone', 'comment'], 'trim'],

            [['name', 'phone', 'product'], 'required'],

            ['phone', PhoneInputValidator::class],
            ['product', 'number'],
            ['product', 'exist', 'skipOnEmpty' => false, 'skipOnError' => false, 'targetClass' => Product::class, 'targetAttribute' => ['product' => 'id']],

            [['comment'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name'    => Yii::t('app', 'Name'),
            'phone'   => Yii::t('app', 'Phone'),
            'comment' => Yii::t('app', 'Comment'),
            'product' => Yii::t('app', 'Product')
        ];
    }

    public function save(): bool
    {
        if ($this->validate()) {
            return (new Order([
                'client_name'  => $this->name,
                'client_phone' => $this->phone,
                'comment'      => $this->comment,
                'product_id'   => $this->product,
                'total'        => Product::findOne($this->product)->price,
            ]))->save();
        }

        return false;
    }
}
