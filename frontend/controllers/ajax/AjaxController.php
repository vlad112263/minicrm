<?php
namespace frontend\controllers\ajax;

class AjaxController extends \common\controllers\ajax\AjaxController
{
    const VALIDATION_URL = 'ajax/ajax/validate';
}
