<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $model \common\models\logic\User
 */

$this->title = 'Editing';
$this->params['breadcrumbs'][] = ['label' => 'List of users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->email, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="role-update">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'email')->textInput() ?>
    <?= $form->field($model, 'password')->textInput() ?>
    <?= $form->field($model, 'role')->dropDownList(\backend\modules\user\models\User::roleLabels()) ?>
    <?= $form->field($model, 'status')->dropDownList(\backend\modules\user\models\User::statusLabels()) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
