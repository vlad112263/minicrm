<?php

use common\helpers\Dropdown;
use common\models\logic\Order;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel \backend\modules\user\models\searches\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'List of users';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="order-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute'=>'id',
                'contentOptions'=>['style'=>'width: 60px;']
            ],
            'email',
            'password',
            [
                'attribute' => 'role',
                'value' => function (\backend\modules\user\models\User $model) {
                    return $model->roleLabel;
                },
                'filter' => \backend\modules\user\models\User::roleLabels()
            ],
            [
                'attribute' => 'status',
                'value' => function (\backend\modules\user\models\User $model) {
                    return $model->statusLabel;
                },
                'filter' => \backend\modules\user\models\User::statusLabels()
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete} {update}',
            ],
        ],
    ]); ?>
</div>
