<?php
namespace backend\modules\user\models;

use common\helpers\ArrayHelper;

class User extends \common\models\logic\User
{
    /**
     * @inheritDoc
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(), [
                [['email', 'password', 'status', 'role'], 'required'],
                ['email', 'email']
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function afterValidate()
    {
        parent::afterValidate();
        $this->password_hash = \Yii::$app->security->generatePasswordHash($this->password);
    }


}
