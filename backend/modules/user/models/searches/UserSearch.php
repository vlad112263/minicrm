<?php
namespace backend\modules\user\models\searches;

use backend\modules\user\models\User;
use yii\data\ActiveDataProvider;


class UserSearch extends User
{

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'role'], 'integer'],
            [['password_hash', 'email'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider(compact('query'));

        $this->load($params);

        if (! $this->validate()) {
            return $dataProvider;
        }

        $query
            ->andFilterWhere(['status' => $this->status])
            ->andFilterWhere(['role' => $this->role])
            ->andFilterWhere(['like', 'email', $this->email]);


        return $dataProvider;
    }

}
