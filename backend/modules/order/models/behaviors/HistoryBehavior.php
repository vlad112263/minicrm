<?php
namespace backend\modules\order\models\behaviors;

use backend\modules\order\models\Order;
use common\models\logic\OrderHistory;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\AfterSaveEvent;


class HistoryBehavior extends Behavior
{
    protected function saveAttributes(): array
    {
        return [
            'client_name',
            'client_phone',
            'product_id',
            'comment',
            'total',
            'created_at',
            'status'
        ];
    }

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate'
        ];
    }

    public function afterUpdate(AfterSaveEvent $event)
    {
        /** @var Order $model */
        $model = $this->owner;

        $oldAttributes = array_intersect_key($event->changedAttributes, array_flip($this->saveAttributes()));

        $newAttributes = array_intersect_key($model->getAttributes(), $oldAttributes);

        return (new OrderHistory([
            'user_id' => \Yii::$app->user->id,
            'order_id' => $model->id,
            'old_data' => $oldAttributes,
            'data'     => $newAttributes
        ]))->save();

    }

}
