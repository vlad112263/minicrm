<?php
namespace backend\modules\order\models;


use backend\modules\order\models\behaviors\HistoryBehavior;
use common\helpers\ArrayHelper;
use yii\db\AfterSaveEvent;

class Order extends \common\models\logic\Order
{
    /**
     * @inheritDoc
     */
    public function afterValidate()
    {
        parent::afterValidate();
        $this->status = (int)$this->status;
        $this->created_at = strtotime($this->created_at);
    }


    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                HistoryBehavior::class
            ]
        );
    }
}
