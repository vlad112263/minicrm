<?php
namespace backend\modules\order\models\searches;

use common\models\logic\OrderHistory;
use common\models\logic\User;
use yii\data\ActiveDataProvider;


class OrderHistorySearch extends OrderHistory
{

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'user_id'], 'integer'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrderHistory::find()->orderBy(['created_at' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider(compact('query'));

        $this->load($params);

        if (! $this->validate()) {
            return $dataProvider;
        }

        $query
            ->andFilterWhere(['user_id'  => $this->user_id])
            ->andFilterWhere(['order_id' => $this->order_id]);


        return $dataProvider;
    }

}
