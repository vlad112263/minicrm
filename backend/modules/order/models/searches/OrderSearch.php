<?php
namespace backend\modules\order\models\searches;

use common\models\logic\Order;
use yii\data\ActiveDataProvider;


class OrderSearch extends Order
{

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'product_id'], 'integer'],
            [['client_name', 'client_phone'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find()->orderBy(['created_at' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider(compact('query'));

        $this->load($params);

        if (! $this->validate()) {
            return $dataProvider;
        }

        $query
            ->andFilterWhere(['status' => $this->status])
            ->andFilterWhere(['product_id' => $this->product_id])
            ->andFilterWhere(['like', 'client_name', $this->client_name])
            ->andFilterWhere(['like', 'client_phone', $this->client_phone]);


        return $dataProvider;
    }

}
