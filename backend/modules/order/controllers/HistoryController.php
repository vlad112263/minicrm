<?php
namespace backend\modules\order\controllers;

use backend\modules\order\models\Order;
use backend\modules\order\models\searches\OrderHistorySearch;
use backend\modules\order\models\searches\OrderSearch;
use common\models\logic\OrderHistory;
use common\models\utils\Request;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class HistoryController extends Controller
{

    /**
     * Lists all order history models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderHistorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', compact('searchModel', 'dataProvider'));
    }

    /**
     * Find order history model by Id
     * @param $id
     * @return OrderHistory|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = OrderHistory::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
