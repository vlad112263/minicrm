<?php
namespace backend\modules\order\controllers;

use backend\modules\order\models\Order;
use backend\modules\order\models\searches\OrderSearch;
use common\helpers\ArrayHelper;
use common\helpers\CsvHelper;
use common\models\utils\Request;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class OrderController extends Controller
{

    /**
     * Lists all order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', compact('searchModel', 'dataProvider'));
    }

    /**
     * Update order model
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Request::isPost() && $model->load(Request::post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', compact('model'));
    }

    /**
     * Export order to csv file
     * @return bool|false|string
     */
    public function actionExport()
    {
        $headers = [
            Yii::t('app', 'Order ID'),
            Yii::t('app', 'Product'),
            Yii::t('app', 'Price'),
            Yii::t('app', 'Phone'),
        ];
        $data = ArrayHelper::map(Order::find()->all(), 'id', function (Order $order) {
            return [
                'id' => $order->id,
                'product' => $order->product->name,
                'price' => $order->total,
                'phone' => '+' . $order->client_phone
            ];
        });
        return CsvHelper::render($headers, $data);
    }

    /**
     * Find order model by ID
     * @param $id
     * @return Order|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
