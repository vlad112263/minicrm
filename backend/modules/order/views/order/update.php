<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $model \common\models\logic\Order
 */

$this->title = Yii::t('app', 'Editing order {order}', ['order' => $model->id] );
$this->params['breadcrumbs'][] = ['label' => 'List of orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="order-update">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'client_name')->textInput() ?>

    <?= $form->field($model, 'client_phone')->widget(\borales\extensions\phoneInput\PhoneInput::class, [
        'jsOptions' => [
            'allowExtensions'    => true,
            'nationalMode'       => false,
            'preferredCountries' => [isset($_SERVER['HTTP_CF_IPCOUNTRY']) ? mb_strtolower($_SERVER['HTTP_CF_IPCOUNTRY']) : 'de'],
        ]
    ]) ?>

    <?= $form->field($model, 'comment')->textarea() ?>

    <?= $form->field($model, 'product_id')->dropDownList(\common\helpers\Dropdown::products()) ?>

    <?= $form->field($model, 'total')->textInput() ?>

    <?= $form->field($model, 'created_at')->widget(\kartik\datetime\DateTimePicker::class, [
        'options' => [
            'value' => Yii::$app->formatter->asDate($model->created_at, 'php:d-m-Y H:i')
        ],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'dd/mm/yyyy hh:ii                                                               '
        ]
    ]) ?>

    <?= $form->field($model, 'status')->dropDownList(\common\helpers\Dropdown::orderStatuses()) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
