<?php

use common\helpers\Dropdown;
use common\models\logic\Order;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel \backend\modules\order\models\searches\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'List of orders';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="order-index">

    <div class="form-group">
        <?=  Html::a('Export csv', Url::to(['/order/order/export']), ['class' => 'btn btn-success']); ?>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute'=>'id',
                'contentOptions'=>['style'=>'width: 60px;']
            ],
            'client_name',
            'client_phone',
            'comment',
            [
                'attribute' => 'product_id',
                'value' => function (Order $order) {
                    return $order->product->name;
                },
                'filter' => Dropdown::products()
            ],
            [
                'attribute' => 'total',
                'value' => function (Order $order) {
                    return Yii::$app->formatter->asCurrency($order->total);
                }
            ],
            [
                'attribute' => 'created_at',
                'label'     => Yii::t('app', 'Created at'),
                'value'     => function (Order $order) {
                    return Yii::$app->formatter->asDate($order->created_at, 'php:d M, Y H:i');
                }
            ],
            [
                'attribute' => 'updated_at',
                'label'     => Yii::t('app', 'Updated at'),
                'value'     => function (Order $order) {
                    return Yii::$app->formatter->asDate($order->updated_at, 'php:d M, Y H:i');
                }
            ],
            [
                'attribute' => 'status',
                'value'  => function (Order $order) {
                    return $order->statusLabel;
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
            ],
        ],
    ]); ?>
</div>
