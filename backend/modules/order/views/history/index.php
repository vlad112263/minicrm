<?php

use common\models\logic\OrderHistory;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel \backend\modules\order\models\searches\OrderHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'List of users';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-index">
    <h1>
        <a href="<?= Url::to(['create']); ?>" class="btn btn-success block right">
            <i class="fa fa-plus" aria-hidden="true"></i> Add
        </a>
    </h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn'
            ],
            [
                'attribute'  => 'user_id',
                'label'      => Yii::t('app', 'User'),
                'value'      => function (OrderHistory $model) {
                    return $model->user->email;
                },
                'filter'     => \common\helpers\Dropdown::users()
            ],
            [
                'attribute'  => 'order_id',
                'label'      => Yii::t('app', 'Order'),
                'value'      => function (OrderHistory $model) {
                    return $model->order->id;
                }
            ],
            [
                'label' => Yii::t('app', 'Changes'),
                'format' => 'raw',
                'value' => function (OrderHistory $model) {
                    $html = [];
                    foreach ($model->old_data as $name => $val) {
                        $label = $model->order->getAttributeLabel($name);
                        $oldVal = $model->getFormattedValue($name, $val);
                        $newVal = $model->getFormattedValue($name, $model->data[$name]);

                        $html []= "<strong>{$label}</strong> : $oldVal => $newVal";
                    }
                    return implode('<br/>', $html);
                }
            ]

        ],
    ]); ?>
</div>
