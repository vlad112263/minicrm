<?php

use common\models\logic\User;
use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id'            => $this->primaryKey(),
            'email'         => $this->string()->notNull()->unique(),
            'password'      => $this->string()->notNull(),
            'password_hash' => $this->string()->notNull(),
            'status'        => $this->smallInteger()->notNull()->defaultValue(User::STATUS_INACTIVE),
            'role'          => $this->smallInteger()->notNull()->defaultValue(User::ROLE_MANAGER),
            'deleted_at'    => $this->integer(),
            'created_at'    => $this->integer()->notNull(),
            'updated_at'    => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%client}}', [
            'id'    => $this->primaryKey(),
            'name'  => $this->string(80)->notNull(),
            'phone' => $this->string(20)->notNull()
        ], $tableOptions);

        $this->createTable('{{%product}}', [
            'id'    => $this->primaryKey(),
            'name'  => $this->string(255)->notNull(),
            'price' => $this->decimal(11, 2),
        ], $tableOptions);

        $this->createTable('{{%order}}', [
            'id'           => $this->primaryKey(),
            'client_name'  => $this->string(80)->notNull(),
            'client_phone' => $this->string(20)->notNull(),
            'comment'      => $this->text(),
            'product_id'   => $this->integer(11)->notNull(),
            'total'        => $this->decimal(11, 2)->notNull(),
            'status'       => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at'   => $this->integer()->notNull(),
            'updated_at'   => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%order_history}}', [
            'id'       => $this->primaryKey(),
            'user_id'  => $this->integer(11)->notNull(),
            'order_id' => $this->integer(11)->notNull(),
            'old_data' => $this->text(),
            'data'     => $this->text(),
            'created_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('{{%FK-order-product}}', '{{%order}}', 'product_id', '{{%product}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('{{%FK-order_history-user}}', '{{%order_history}}', 'user_id', '{{%user}}', 'id', 'NO ACTION', 'CASCADE');
        $this->addForeignKey('{{%FK-order_history-order}}', '{{%order_history}}', 'order_id', '{{%order}}', 'id', 'NO ACTION', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('{{%FK-order-product}}', '{{%order}}');

        $this->dropForeignKey('{{%FK-order_history-user}}', '{{%order_history}}');
        $this->dropForeignKey('{{%FK-order_history-order}}', '{{%order_history}}');

        $this->dropIndex('{{%IDX_order_product}}', '{{%order_product}}');
        $this->dropTable('{{%order_product}}');
        $this->dropTable('{{%order_history}}');
        $this->dropTable('{{%order}}');
        $this->dropTable('{{%product}}');
        $this->dropTable('{{%client}}');
        $this->dropTable('{{%user}}');
    }
}
