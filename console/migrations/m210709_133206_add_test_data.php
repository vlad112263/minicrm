<?php

use common\models\logic\Product;
use yii\db\Migration;

/**
 * Class m210709_133206_add_test_data
 */
class m210709_133206_add_test_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $products = [
            [
                'name' => 'Апельсин',
                'price' => 15.25
            ],
            [
                'name' => 'Яблоко',
                'price' => 25.99
            ],
            [
                'name' => 'Груша',
                'price' => 32.85
            ],
            [
                'name' => 'Шоколад',
                'price' => 7.99
            ]
        ];

        $users = [
            ['email' => 'vlad112263@gmail.com', 'password' => '1234qwer'],
            ['email' => 'poll696@mail.ru', 'password' => '1234qwer'],
            ['email' => 'poll123@mail.ru', 'password' => '1234qwer'],
        ];

        foreach ($products as $product) {
            $this->insert('{{%product}}', $product);
        }

        foreach ($users as $user) {
            $this->insert('{{%user}}', $user + [
                'password_hash' => Yii::$app->security->generatePasswordHash($user['password']),
                'status' => \common\models\logic\User::STATUS_ACTIVE,
                'created_at' => time(),
                'updated_at' => time(),
            ]);
        }


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        Product::deleteAll();
    }
}
